using CallendarApi.ApiModels;
using CallendarApi.Controllers;
using CallendarApi.Data;
using CallendarApi.Helpers;
using CallendarApi.Interfaces;
using CallendarApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Tests
{
    public class TestCallendar
    {
        EventController controller;
        IServiceCollection services;
        IServiceProvider provider;
        ILogger<EventController> logger;
        string locId;
        DbContextOptions<CallendarDbContext> opt;

        [SetUp]
        public void Setup()
        {
            opt = new DbContextOptionsBuilder<CallendarDbContext>().UseInMemoryDatabase(databaseName: "CallendarDb").Options;
            logger = new Logger<EventController>(new LoggerFactory());
            services = new ServiceCollection();
            services.AddControllers();
            services.AddDbContext<CallendarDbContext>(options => options.UseInMemoryDatabase(databaseName: "CallendarDb"));
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<CallendarDbContext>()
                .AddDefaultTokenProviders();
            services.AddLogging();
            provider = services.BuildServiceProvider();
            controller = new EventController(provider);


            //Mocking db in memmory with not random information.

            SeedDb.NotRandomInitialize(provider);

            using (var db = new CallendarDbContext(opt))
            {
                Random rand = new Random();
                var users = db.Users.Select(u => u).ToList();

                var conferenceRooms = db.ConferenceRooms.ToList();
                locId = conferenceRooms[3].Id.ToString();

                var identity = new ClaimsIdentity("AuthenticationTypes.Federation");

                identity.AddClaim(
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", users[2].Id));
                identity.AddClaim(new Claim("jti", "0277422a-4a33-4248-bc57-5c7dea3d2d67"));
                identity.AddClaim(new Claim("exp", "9999999999"));
                identity.AddClaim(new Claim("iss", "http://mariuszlewczuk.com"));
                identity.AddClaim(new Claim("aud", "https://tango.agency/"));

                var principal = new GenericPrincipal(identity, null);

                controller.ControllerContext = new ControllerContext()
                {
                    HttpContext = new DefaultHttpContext() { User = principal }
                };
            }
        }


        [Test]
        public async Task TestAddingEvent()
        {
            string name = "TestMeeting";
            string agenda = "This is the test meeting";
            DateTimeOffset start = DateTimeOffset.Parse("2021-05-14T12:51:31.540Z");
            DateTimeOffset end = DateTimeOffset.Parse("2021-05-14T20:51:31.540Z");
            List<string> participants = new List<string>
            {
                "User1@user.com",
                "User8@user.com"
            };
            string locationId = "Room2";

            var model = new EventModel() { Name = name, Agenda = agenda, Start = start, End = end, LocationId = locationId, Participants = participants };

            var response = await controller.Create(model);
            var result = response as BadRequestObjectResult;
            Assert.AreEqual(StatusCodes.Status400BadRequest, result.StatusCode);

            model.LocationId = locId;
            response = await controller.Create(model);
            var okResult = response as OkResult;
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);

        }
        [Test]
        public void TestEventSelector()
        {
            using (var db = new CallendarDbContext(opt))
            {
                //Test types of Events.
                IEnumerable<IEventModel> events = db.CallendarEvents
                        .Include(c => c.Participants).ThenInclude(p => p.User)
                        .Include(e => e.Owner)
                        .Include(c => c.Location).ThenInclude(l => l.Owner).Select(e => new ReturnEventModel(e, TimeZoneInfo.Local.Id));
                EventSelector eS = new EventSelector(events);
                Assert.AreEqual(typeof(EntityQueryable<ReturnEventModel>), eS._events.GetType());

                IEnumerable<IEventModel> extendedEvents = db.CallendarEvents
                        .Include(c => c.Participants).ThenInclude(p => p.User)
                        .Include(e => e.Owner)
                        .Include(c => c.Location).ThenInclude(l => l.Owner).Select(e => new ExtendedEventModel(e, TimeZoneInfo.Local.Id));
                eS = new EventSelector(extendedEvents);
                Assert.AreEqual(typeof(EntityQueryable<ExtendedEventModel>), eS._events.GetType());

                //Test selection by day.
                eS.SelectByDay("2021-05-08");
                Assert.AreEqual(3, eS._events.Count());

                //Test selection by location
                eS = new EventSelector(extendedEvents);
                eS.SelectByLocation(locId);
                Assert.AreEqual(7, eS._events.Count());

                //Test query searching
                eS = new EventSelector(extendedEvents);
                eS.SearchByQuery("Event22");
                Assert.AreEqual(1, eS._events.Count());

                eS = new EventSelector(extendedEvents);
                eS.SearchByQuery("Event22 Event4");
                Assert.AreEqual(2, eS._events.Count());

                eS = new EventSelector(extendedEvents);
                eS.SearchByQuery("Event22 Event14 Event16");
                Assert.AreEqual(3, eS._events.Count());

                //Shows that adding same word does not add same instances
                eS = new EventSelector(extendedEvents);
                eS.SearchByQuery("Event22 Event3 Event4 Event4 Event5 Event5 Event6");
                Assert.AreEqual(5, eS._events.Count());

                //Selects each Event with Event2 in the name or agenda. 
                eS = new EventSelector(extendedEvents);
                eS.SearchByQuery("Event2");
                Assert.AreEqual(6, eS._events.Count());

                eS = new EventSelector(extendedEvents);
                eS.SearchByQuery("");
                Assert.AreEqual(26, eS._events.Count());
            }
        }
    }
}