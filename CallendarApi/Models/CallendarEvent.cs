﻿using CallendarApi.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Models
{
    public class CallendarEvent
    {

        [Key]
        public Guid Id { get; set; }
        virtual public string OwnerId { get; set; }
        public ApplicationUser Owner { get; set; }
        public string Name { get; set; }
        public string Agenda { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }
        public ICollection<UserEvent> Participants { get; set; }
        public Guid LocationId { get; set; }
        public ConferenceRoom Location { get; set; }
    }
}
