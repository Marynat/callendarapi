﻿using CallendarApi.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Models
{
    public class ConferenceRoom
    {
        [Key]
        public Guid Id { get; set; }
        public ApplicationUser Owner { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
