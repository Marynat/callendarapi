﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Models
{
    public class UserEvent
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public Guid EventId { get; set; }
        public CallendarEvent Event { get; set; }
    }
}
