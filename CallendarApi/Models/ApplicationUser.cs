﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Models
{
    public class ApplicationUser : IdentityUser
    {
        public Guid Company_id { get; set; }
        public string TimeZone { get; set; }

        public ICollection<UserEvent> UserEvents { get; set; }
    }
}
