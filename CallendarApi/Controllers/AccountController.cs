﻿using CallendarApi.Data;
using CallendarApi.ApiModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CallendarApi.Models;

namespace CallendarApi.Controllers
{
    public class AccountController : ControllerBase
    {
        private readonly ILogger<EventController> _logger;
        private UserManager<ApplicationUser> _uManager;

        public AccountController(ILogger<EventController> logger, UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            _uManager = userManager;
        }

        /// <summary>
        /// Login method using JWT token and authentication
        /// </summary>
        /// <param name="model">Takes JSON wit UserName and Password</param>
        /// <returns></returns>
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await _uManager.FindByNameAsync(model.Username);
            if (user != null && await _uManager.CheckPasswordAsync(user, model.Password) && !await _uManager.IsLockedOutAsync(user))
            {
                var authClaims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var authKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("excitedToWorkForTango"));
                var token = new JwtSecurityToken(
                        issuer: "http://mariuszlewczuk.com",
                        audience: "https://tango.agency/",
                        expires: DateTime.Now.AddHours(5),
                        claims: authClaims,
                        signingCredentials: new SigningCredentials(authKey, SecurityAlgorithms.HmacSha256)
                    );
                await _uManager.ResetAccessFailedCountAsync(user);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            if (_uManager.SupportsUserLockout && await _uManager.GetLockoutEnabledAsync(user))
            {
                if (await _uManager.GetAccessFailedCountAsync(user) < 2)
                {
                    await _uManager.SetLockoutEndDateAsync(user, DateTime.MaxValue);
                    await _uManager.ResetAccessFailedCountAsync(user);
                    return Problem("", "", 423, "You are locked out", "Locked");
                }
                else
                {
                    await _uManager.AccessFailedAsync(user);
                }
            }
            return Unauthorized();

        }
    }
}
