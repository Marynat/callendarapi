﻿using CallendarApi.ApiModels;
using CallendarApi.Data;
using CallendarApi.Helpers;
using CallendarApi.Interfaces;
using CallendarApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Headers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CallendarApi.Controllers
{
    /// <summary>
    /// Controller for callendar events
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class EventController : ControllerBase
    {
        private IServiceProvider _serviceProvider;

        public EventController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Controller method to create a callendar event
        /// </summary>
        /// <param name="_event">
        /// Takes Json: 
        /// 
        /// </param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] EventModel _event)
        {

            var db = _serviceProvider.GetRequiredService<CallendarDbContext>();
            var uManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var user = await uManager.GetUserAsync(User);
            Guid loc = Guid.Empty;

            try
            {
                loc = db.ConferenceRooms.Where(l => l.Id == Guid.Parse(_event.LocationId)).Select(l => l.Id).FirstOrDefault();
                if (loc == Guid.Empty)
                    return BadRequest("Location (Conference Room) with this Id does not exist, also make sure your Id is correct");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Location (Conference Room) with this Id does not exist, also make sure your Id is correct");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }


            var tmpStart = _event.Start.AddHours(8);
            if (tmpStart.CompareTo(_event.End) < 0)
                return Problem("Meeting cannot be longer than 8 hours", "PostEvent", 422, "Long meeting", "Unprocessable Entity");

            var ev = new CallendarEvent() { Name = _event.Name, Agenda = _event.Agenda, OwnerId = user.Id, LocationId = loc, Start = _event.Start, End = _event.End };
            db.CallendarEvents.Add(ev);
            foreach (var p in _event.Participants)
            {
                var part = db.Users.Where(x => x.Email == p).Select(x => x.Id).First();

                db.UserEvents.Add(new UserEvent() { UserId = part, EventId = ev.Id });
            }
            db.SaveChanges();

            return Ok();

        }

        /// <summary>
        /// Method to retrive events for currently logged user
        /// </summary>
        /// <param name="day">Filter events by day</param>
        /// <param name="location_id">Filter events by location</param>
        /// <param name="query">Search in name or agenda</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("Retrive")]
        public async Task<IActionResult> Retrive(string day, string location_id, string query)
        {
            var db = _serviceProvider.GetRequiredService<CallendarDbContext>();
            var uManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            ClaimsPrincipal cp = this.User;
            var user = await uManager.GetUserAsync(cp);

            try
            {
                IEnumerable<IEventModel> events = db.CallendarEvents.Where(c => c.Participants.Any(p => p.User.Id == user.Id) || c.Owner.Id == user.Id || c.Location.Owner.Id == user.Id)
                    .Include(c => c.Participants).ThenInclude(p => p.User)
                    .Include(e => e.Owner)
                    .Include(c => c.Location).ThenInclude(l => l.Owner).Select(e => new ReturnEventModel(e, user.TimeZone));

                EventSelector eh = new EventSelector(events);
                eh.SelectByDay(day)
                  .SelectByLocation(location_id)
                  .SearchByQuery(query);
                return Ok(eh._events);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Retrive more details of an event
        /// </summary>
        /// <param name="id">Id of retrived event</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("RetriveDetails")]
        public async Task<IActionResult> RetriveDetails(string id)
        {
            var db = _serviceProvider.GetRequiredService<CallendarDbContext>();
            var uManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var user = await uManager.GetUserAsync(User);

            ExtendedEventModel model = new ExtendedEventModel();
            try
            {
                model = db.CallendarEvents.Where(c => c.Id == Guid.Parse(id)).Include(e => e.Participants).ThenInclude(p => p.User).Include(e => e.Owner).Include(e => e.Location).Select(e => new ExtendedEventModel(e, user.TimeZone)).FirstOrDefault();
                if (model == null)
                    return BadRequest("Event with this Id does not exist, make sure your Id is correct");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Location (Conference Room) with this Id does not exist, make sure your Id is correct");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(model);
        }

    }
}
