﻿using CallendarApi.ApiModels;
using CallendarApi.Data;
using CallendarApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoomController : ControllerBase
    {
        private IServiceProvider _serviceProvider;

        public RoomController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Create conference room
        /// </summary>
        /// <param name="model">Takes Json:</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] InputLocationModel model)
        {

            var db = _serviceProvider.GetRequiredService<CallendarDbContext>();
            ApplicationUser owner = new ApplicationUser();
            if (string.IsNullOrEmpty(model.OwnerId))
            {
                var uManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                owner = await uManager.GetUserAsync(User);
            }
            else
            {
                owner = db.Users.FirstOrDefault(u => u.Id == model.OwnerId);
                if (owner == null)
                    return BadRequest("User with this id doesn't exist");
            }

            db.Add(new ConferenceRoom() { Address = model.Address, Name = model.Name, Owner = owner });
            db.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Method to retrive information about specyfic conference room
        /// </summary>
        /// <param name="id">Id if conference room</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("Retrive")]
        public IActionResult Retrive(string id)
        {

            var db = _serviceProvider.GetRequiredService<CallendarDbContext>();
            ReturnLocationModel cr = new ReturnLocationModel();
            try
            {
                cr = db.ConferenceRooms.Include(r => r.Owner).Where(r => r.Id == Guid.Parse(id)).Select(r => new ReturnLocationModel(r)).FirstOrDefault();
                if (cr == null)
                    return BadRequest("Location (Conference Room) with this Id does not exist, make sure your Id is correct");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Location (Conference Room) with this Id does not exist, make sure your Id is correct");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(cr);
        }
    }
}
