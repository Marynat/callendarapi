﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Interfaces
{
    public interface IEventModel
    {
        string Name { get; set; }
        string Agenda { get; set; }
        DateTimeOffset Start { get; set; }
        DateTimeOffset End { get; set; }
        IEnumerable<string> Participants { get; set; }
        string LocationId { get; set; }
    }
}
