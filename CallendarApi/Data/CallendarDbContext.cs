﻿using CallendarApi.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Data
{
    public class CallendarDbContext : IdentityDbContext<ApplicationUser>
    {
        public CallendarDbContext(DbContextOptions<CallendarDbContext> options) : base(options)
        {
        }
        public DbSet<ConferenceRoom> ConferenceRooms { get; set; }
        public DbSet<CallendarEvent> CallendarEvents { get; set; }
        public DbSet<UserEvent> UserEvents { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserEvent>().HasKey(ue => new { ue.EventId, ue.UserId });
            builder.Entity<UserEvent>()
                .HasOne(ue => ue.User)
                .WithMany(u => u.UserEvents)
                .HasForeignKey(ue => ue.UserId);
            builder.Entity<UserEvent>()
                .HasOne(ue => ue.Event)
                .WithMany(e => e.Participants)
                .HasForeignKey(ue => ue.EventId);
            base.OnModelCreating(builder);
        }
    }
}
