﻿using CallendarApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CallendarApi.Data
{
    public class SeedDb
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var db = serviceProvider.GetRequiredService<CallendarDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            db.Database.EnsureCreated();
            Random rand = new Random();
            if (!db.Users.Any())
            {
                var timezones = TimeZoneInfo.GetSystemTimeZones();
                List<Guid> companies = new List<Guid>();
                companies.Add(Guid.NewGuid());
                companies.Add(Guid.NewGuid());
                companies.Add(Guid.NewGuid());
                for (int i = 0; i < 10; i++)
                {
                    ApplicationUser user = new ApplicationUser() { Email = "User" + i + "@user.com", UserName = "Test" + i, Company_id = companies[rand.Next(3)], TimeZone = timezones[rand.Next(141)].Id };
                    userManager.CreateAsync(user, "Test@123");
                    //Thread.Sleep(50);
                }

            }
            var users = db.Users.ToList();
            List<ConferenceRoom> conferenceRooms = new List<ConferenceRoom>();

            if (!db.ConferenceRooms.Any())
            {
                for (int i = 0; i < 5; i++)
                {
                    ConferenceRoom conferenceRoom = new ConferenceRoom() { Id = Guid.NewGuid(), Address = "City" + i + "Street Nr" + i, Name = "Room" + i, Owner = users[rand.Next(10)] };
                    conferenceRooms.Add(conferenceRoom);
                    db.ConferenceRooms.Add(conferenceRoom);
                }
            }

            if (!db.CallendarEvents.Any())
            {
                if (!conferenceRooms.Any())
                {
                    conferenceRooms = db.ConferenceRooms.ToList();
                }
                for (int i = 0; i < 25; i++)
                {
                    int numOfPart = rand.Next(8);
                    var currentParticipants = new List<ApplicationUser>();
                    for (int j = 0; j < numOfPart; j++)
                    {
                        ApplicationUser randUser = users[rand.Next(10)];
                        if (!currentParticipants.Any(c => c.Id == randUser.Id))
                            currentParticipants.Add(randUser);
                    }
                    DateTimeOffset startDate = DateTimeOffset.UtcNow.AddDays(-10).AddDays(rand.Next(20)).AddHours(rand.Next(24));
                    CallendarEvent callendarEvent = new CallendarEvent() { Id = Guid.NewGuid(), Name = "Event" + i, Agenda = "Topic " + i, Owner = users[rand.Next(10)], Location = conferenceRooms[rand.Next(5)], Start = startDate, End = startDate.AddHours(rand.Next(8)) };
                    db.CallendarEvents.Add(callendarEvent);
                    foreach (var participant in currentParticipants)
                    {
                        db.UserEvents.Add(new UserEvent() { Event = callendarEvent, EventId = callendarEvent.Id, User = participant, UserId = participant.Id });
                    }
                }
            }
            db.SaveChanges();
        }
        public static void NotRandomInitialize(IServiceProvider serviceProvider)
        {
            var db = serviceProvider.GetRequiredService<CallendarDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            db.Database.EnsureCreated();
            if (!db.Users.Any())
            {
                var timezones = TimeZoneInfo.GetSystemTimeZones();
                List<Guid> companies = new List<Guid>();
                companies.Add(Guid.NewGuid());
                companies.Add(Guid.NewGuid());
                companies.Add(Guid.NewGuid());
                for (int i = 0; i < 10; i++)
                {
                    ApplicationUser user = new ApplicationUser() { Email = "User" + i + "@user.com", UserName = "Test" + i, Company_id = companies[i%2], TimeZone = timezones[i*7%141].Id };
                    userManager.CreateAsync(user, "Test@123");
                }

            }
            var users = db.Users.ToList();
            List<ConferenceRoom> conferenceRooms = new List<ConferenceRoom>();

            if (!db.ConferenceRooms.Any())
            {
                for (int i = 0; i < 5; i++)
                {
                    ConferenceRoom conferenceRoom = new ConferenceRoom() { Id = Guid.NewGuid(), Address = "City" + i + "Street Nr" + i, Name = "Room" + i, Owner = users[i%9] };
                    conferenceRooms.Add(conferenceRoom);
                    db.ConferenceRooms.Add(conferenceRoom);
                }
            }

            if (!db.CallendarEvents.Any())
            {
                if (!conferenceRooms.Any())
                {
                    conferenceRooms = db.ConferenceRooms.ToList();
                }
                for (int i = 0; i < 25; i++)
                {
                    int numOfPart = i%8;
                    var currentParticipants = new List<ApplicationUser>();
                    for (int j = 0; j < numOfPart; j++)
                    {
                        ApplicationUser randUser = users[i%9];
                        if (!currentParticipants.Any(c => c.Id == randUser.Id))
                            currentParticipants.Add(randUser);
                    }
                    DateTimeOffset startDate = DateTimeOffset.UtcNow.AddDays(-10).AddDays(i%9).AddHours(i%9);
                    CallendarEvent callendarEvent = new CallendarEvent() { Id = Guid.NewGuid(), Name = "Event" + i, Agenda = "Topic " + i, Owner = users[i%9], Location = conferenceRooms[i%4], Start = startDate, End = startDate.AddHours(i%7) };
                    db.CallendarEvents.Add(callendarEvent);
                    foreach (var participant in currentParticipants)
                    {
                        db.UserEvents.Add(new UserEvent() { Event = callendarEvent, EventId = callendarEvent.Id, User = participant, UserId = participant.Id });
                    }
                }
            }
            db.SaveChanges();
        }
    }
}
