﻿using CallendarApi.ApiModels;
using CallendarApi.Exceptions;
using CallendarApi.Interfaces;
using CallendarApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Helpers
{
    public class EventSelector
    {
        public IEnumerable<IEventModel> _events;

        public EventSelector(IEnumerable<IEventModel> events)
        {
            _events = events;
        }

        /// <summary>
        /// Method to filter events by day
        /// </summary>
        /// <param name="day">day string</param>
        /// <returns></returns>
        public EventSelector SelectByDay(string day)
        {
            if (!string.IsNullOrEmpty(day))
            {
                DateTimeOffset dateTime = new DateTimeOffset();
                if (DateTimeOffset.TryParse(day, out dateTime))
                {
                    _events = _events.Where(c => c.Start.Date == dateTime.Date);
                }
                else throw new WrongDateFromatException();
            }
            return this;
        }

        /// <summary>
        /// Method to filter events by location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public EventSelector SelectByLocation(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                try
                {
                    _events = _events.Where(c => c.LocationId == location);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return this;
        }

        /// <summary>
        /// Search event with words in query
        /// </summary>
        /// <param name="query">Strin with words divided by "space"</param>
        /// <returns></returns>
        public EventSelector SearchByQuery(string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                var queries = query.Split(" ");
                _events = _events.Where(e => queries.Any(x => e.Agenda.Contains(x) || e.Name.Contains(x)));
            }
            return this;
        }
    }
}
