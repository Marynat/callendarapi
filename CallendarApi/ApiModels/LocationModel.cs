﻿using CallendarApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.ApiModels
{
    public class LocationModel
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public LocationModel() { }
        public LocationModel(ConferenceRoom room)
        {
            Name = room.Name;
            Address = room.Address;
        }
    }

    public class InputLocationModel : LocationModel
    {
        public string OwnerId { get; set; }

        public InputLocationModel() { }
        public InputLocationModel(ConferenceRoom conferenceRoom) : base(conferenceRoom)
        {
            OwnerId = conferenceRoom.Owner.Id;
        }
    }

    public class ReturnLocationModel : LocationModel
    {
        public UserModel OwnerId { get; set; }

        public ReturnLocationModel() { }
        public ReturnLocationModel(ConferenceRoom conferenceRoom) : base(conferenceRoom)
        {
            OwnerId = new UserModel(conferenceRoom.Owner);
        }
    }
}
