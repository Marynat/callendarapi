﻿using CallendarApi.Helpers;
using CallendarApi.Interfaces;
using CallendarApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.ApiModels
{
    public class EventModel : IEventModel
    {
        private string name;
        private string agenda;
        private DateTimeOffset start;
        private DateTimeOffset end;
        private IEnumerable<string> participants;
        private string locationId;

        public string Name { get => name; set => name = value; }
        public string Agenda { get => agenda; set => agenda = value; }
        public DateTimeOffset Start { get => start; set => start = TimeZoneInfo.ConvertTime(value,TimeZoneInfo.Utc); }
        public DateTimeOffset End { get => end; set => end = TimeZoneInfo.ConvertTime(value, TimeZoneInfo.Utc); }
        public IEnumerable<string> Participants { get => participants; set => participants = value; }
        public string LocationId { get => locationId; set => locationId = value; }

        public EventModel() { }
        public EventModel(CallendarEvent callendarEvent)
        {
            Name = callendarEvent.Name;
            Agenda = callendarEvent.Agenda;
            Start = callendarEvent.Start;
            End = callendarEvent.End;
            Participants = callendarEvent.Participants.Select(p => p.User.Email).ToList();
            LocationId = callendarEvent.LocationId.ToString();
        }
    }
    public class ReturnEventModel : IEventModel
    {
        private string name;
        private string agenda;
        private DateTimeOffset start;
        private DateTimeOffset end;
        private IEnumerable<string> participants;
        private string locationName;
        private string userTimeZone;
        private string locationId;
        public string Id { get; set; }
        public string Owner { get; set; }
        public string Name { get => name; set => name = value; }
        public string Agenda { get => agenda; set => agenda = value; }
        public DateTimeOffset Start { get => TimeZoneInfo.ConvertTime(start,TimeZoneInfo.FindSystemTimeZoneById(userTimeZone)); set => start = value; }
        public DateTimeOffset End { get => TimeZoneInfo.ConvertTime(end, TimeZoneInfo.FindSystemTimeZoneById(userTimeZone)); set => end = value; }
        public IEnumerable<string> Participants { get => participants; set => participants = value; }
        public string LocationName { get => locationName; set => locationName = value; }
        public string LocationId { get => locationId; set => locationId = value; }
        public string LocationOwner { get; set; }

        public ReturnEventModel() { }
        public ReturnEventModel(CallendarEvent callendarEvent, string timeZone)
        {
            Id = callendarEvent.Id.ToString();
            Owner = callendarEvent.Owner.Email;
            Name = callendarEvent.Name;
            Agenda = callendarEvent.Agenda;
            Start = callendarEvent.Start;
            End = callendarEvent.End;
            Participants = callendarEvent.Participants.Select(p => p.User.Email).ToList();
            LocationName = callendarEvent.Location.Name;
            LocationOwner = callendarEvent.Location.Owner.Email;
            LocationId = callendarEvent.LocationId.ToString();
            userTimeZone = timeZone;
        }
    }

    public class ExtendedEventModel : IEventModel
    {
        #region Privates
        private string name;
        private string agenda;
        private DateTimeOffset start;
        private DateTimeOffset end;
        private IEnumerable<string> participants;
        private string locationName;
        private string userTimeZone;
        private string locationId;
        #endregion

        public string Id { get; set; }
        public UserModel Owner { get; set; }
        public string Name { get => name; set => name = value; }
        public string Agenda { get => agenda; set => agenda = value; }
        public DateTimeOffset Start { get => TimeZoneInfo.ConvertTime(start, TimeZoneInfo.FindSystemTimeZoneById(userTimeZone)); set => start = value; }
        public DateTimeOffset End { get => TimeZoneInfo.ConvertTime(end, TimeZoneInfo.FindSystemTimeZoneById(userTimeZone)); set => end = value; }
        public IEnumerable<string> Participants { get => participants; set => participants = value; }
        public string LocationId { get => locationId; set => locationId = value; }
        public string LocationName { get => locationName; set => locationName = value; }
        public LocationModel Location { get; set; }
        public IEnumerable<UserModel> ParticipantInfos { get; set; }

        public ExtendedEventModel() { }
        public ExtendedEventModel(CallendarEvent callendarEvent, string timeZone)
        {
            Id = callendarEvent.Id.ToString();
            Owner = new UserModel(callendarEvent.Owner);
            Name = callendarEvent.Name;
            Agenda = callendarEvent.Agenda;
            Start = callendarEvent.Start;
            End = callendarEvent.End;
            Participants = callendarEvent.Participants.Select(p => p.User.Email).ToList();
            LocationId = callendarEvent.LocationId.ToString();
            LocationName = callendarEvent.Location.Name;
            Location = new LocationModel(callendarEvent.Location);
            ParticipantInfos = callendarEvent.Participants.Select(p => new UserModel(p.User));
            userTimeZone = timeZone;
        }
    }


}
