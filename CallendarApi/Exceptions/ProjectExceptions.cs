﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CallendarApi.Exceptions
{
    public class WrongDateFromatException : Exception
    {
        public override string Message { get => "Invalid date format"; }
    }
}
