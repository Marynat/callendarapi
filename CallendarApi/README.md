﻿# CallendarApi

https://gitlab.com/Marynat/callendarapi

## Instalation
Change your dbPath in appsettings.json to db File or MSSQL Db. 
Go to CallendarApi -> Properties -> Build -> uncheck and check XML documentation file.
If it is new db run update-database in Package Manager Console to create tables.
Build application and run.

## Usage

Account
Login
/login

Body
```JSON
{
  "username": "string",
  "password": "string"
}
```

Events
Create
/api/Event/Create

Body
```JSON
{
  "name": "string",
  "agenda": "string",
  "start": "2021-05-18T08:57:14.510Z",
  "end": "2021-05-18T08:57:14.510Z",
  "participants": [
    "string",
    "string""
  ],
  "locationId": "string"
}
```
Retrive
/api/Event/Retrive

Params
Example:
```
day - '2021-05-17' or null
location_id - 'aaaa1111-bb22-cc33-dd44-eeeeee555555' or null   
queries - 'fun+serious' or null
```

RetriveDetails
/api/Event/RetriveDetails

Params
Example:
```
Id - 'aaaa1111-bb22-cc33-dd44-eeeeee555555'      
```

## Author
Mariusz Lewczuk.

## License
[This project is licensed under the MIT License](https://choosealicense.com/licenses/mit/)
